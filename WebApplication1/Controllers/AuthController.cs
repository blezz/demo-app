﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MovieFavorites.Data.Models;
using MovieFavoritesAPI.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MovieFavoritesAPI.Controllers {
    [Route("api/auth")]
    public class AuthController : Controller {
        private UserManager<ApplicationUser> userManager;
        private SignInManager<ApplicationUser> signInManager;
        private JwtIssuerSettings jwtIssuerSettings;
        private RoleManager<IdentityRole> roleManager;

        public AuthController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager,
            IOptionsSnapshot<JwtIssuerSettings> jwtIssuerSettings, RoleManager<IdentityRole> roleManager) {
            this.userManager = userManager;
            this.jwtIssuerSettings = jwtIssuerSettings.Value;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }


        [AllowAnonymous]
        [Route("token")]
        [HttpPost]
        public async Task<IActionResult> GenerateToken([FromBody] LoginViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var user = await userManager.FindByEmailAsync(model.Email);

                    if (user != null) {
                        var result = await signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                        if (result.Succeeded) {

                            var token = await CreateJwtTokenAsync(user);
                            return Ok(new { token, validFor = jwtIssuerSettings.ValidFor, RenewAt = jwtIssuerSettings.RenewAt });
                        }
                    }
                }
                catch (Exception e) {
                    return BadRequest(e.Message);
                }
            }

            return BadRequest("Cannot create token or invalid credentials");
        }


        private async Task<string> CreateJwtTokenAsync(ApplicationUser user) {
            var claims = new List<Claim>(new[]
            {
                // Issuer
                new Claim(JwtRegisteredClaimNames.Iss, jwtIssuerSettings.Issuer),   

                // UserName
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),       

                // Email is unique
                new Claim(JwtRegisteredClaimNames.Email, user.Email),        

                // Unique Id for all Jwt tokes
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            });
            claims.AddRange(await userManager.GetClaimsAsync(user));

            // Add user role, they are converted to claims
            var roleNames = await userManager.GetRolesAsync(user);
            foreach (var roleName in roleNames) {
                // Find IdentityRole by name
                var role = await roleManager.FindByNameAsync(roleName);
                if (role != null) {
                    // Convert Identity to claim and add 
                    var roleClaim = new Claim(ClaimTypes.Role, role.Name, ClaimValueTypes.String, jwtIssuerSettings.Issuer);
                    claims.Add(roleClaim);

                    // Add claims belonging to the role
                    var roleClaims = await roleManager.GetClaimsAsync(role);
                    claims.AddRange(roleClaims);
                }
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtIssuerSettings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(jwtIssuerSettings.Issuer,
                jwtIssuerSettings.Issuer,
                claims,
                expires: DateTime.Now.AddSeconds(jwtIssuerSettings.ValidFor),
                signingCredentials: creds);

            var result = new JwtSecurityTokenHandler().WriteToken(token);
            return result;
        }

        public class JwtIssuerSettings {
            public string Issuer { get; set; }
            public int ValidFor { get; set; }
            public int RenewAt { get; set; }
            public string SecretKey { get; set; }
        }

    }
}
