﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieFavorites.Data.Entities;
using MovieFavorites.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace WebApplication1.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class FavoritesController : ControllerBase {
        private readonly IMovieRepository movieRepository;
        private readonly string userId;

        public FavoritesController(IMovieRepository movieRepository, IHttpContextAccessor httpContextAccessor) {
            this.movieRepository = movieRepository;
            
            // resolve the current user email and use it as userId
            userId = httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Email);
        }

        // GET api/favorites
        [Authorize]
        [HttpGet]
        public ActionResult<IEnumerable<Movie>> Get() {
            var result = (List<Movie>) movieRepository.GetUserMovies(userId);
            return result;
        }

        // GET api/favorites/[guid]
        [Authorize]
        [HttpGet("{id}")]
        public ActionResult<Movie> Get(string id) {
            return movieRepository.GetUserMovie(userId, id);
        }

        // POST api/favorites
        [Authorize]
        [HttpPost]
        public void Post([FromBody] Movie movie) {
            movieRepository.AddUserMovie(movie);
        }

        // PUT api/favorites/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody] string value) {
            // TODO: to be implemented
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id) {
            movieRepository.DeleteUserMovie(userId, id);
        }
    }
}
