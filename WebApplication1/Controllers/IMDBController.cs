﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieFavorites.WebAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class IMDBController : ControllerBase {

        [AllowAnonymous]
        [HttpGet]
        [Produces("application/json")]
        public ActionResult Get(string query) {
            var client = new RestClient();

            if (string.IsNullOrEmpty(query)) {
                return BadRequest();
            }

            var request = new RestRequest("http://www.omdbapi.com/");
            request.Method = Method.GET;
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            request.AddParameter("s", query ?? "");
            request.AddParameter("apikey", "9e84a95a");

            var response = client.Execute(request);

            var encoding = Encoding.GetEncoding("utf-8");
            var result = response != null ? encoding.GetString(response.RawBytes) : "";
            
            return Content(result, "application/json");
        }
    }
}
