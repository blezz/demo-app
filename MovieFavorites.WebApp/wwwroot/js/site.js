﻿var closeButtons = Array.from(document.querySelectorAll('.modal .close-button'));

closeButtons.forEach(button => {
    button.addEventListener('click', e => {
        e.target.closest('.modal').classList.remove('is-active');
    });
});


