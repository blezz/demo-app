﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieFavorites.Data.Models {
    public class LoginViewModel {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
