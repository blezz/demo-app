﻿using System;

namespace MovieFavorites.Data.Models {
    public class MovieSearchModel {
        string Title { get; set; }
        string Actor { get; set; }
        DateTime? YearReleased { get; set; }
    }
}
