﻿using MongoDB.Driver;
using MovieFavorites.Data.Entities;
using System.Collections.Generic;

namespace MovieFavorites.Data.Repositories {
    public class MovieRepository : IMovieRepository {
        private IMongoCRUD db;
        private const string COLLECTION_NAME = "user_movies";

        public MovieRepository(IMongoCRUD db) {
            this.db = db;
        }

        // GET
        public IEnumerable<Movie> GetUserMovies(string userId) {
            var result = db.ListByField<Movie>(COLLECTION_NAME, "user_id", userId);
            return result;
        }

        // GET by id
        public Movie GetUserMovie(string userId, string movieId) {
            var filter = Builders<Movie>.Filter.Eq(x => x.UserId, userId) & 
                Builders<Movie>.Filter.Eq(x => x.Id, movieId);

            var result = db.Find<Movie>(COLLECTION_NAME, filter);
            return result;
        }

        // POST
        public void AddUserMovie(Movie movie) {
            db.Insert(COLLECTION_NAME, movie);
        }

        // DELETE
        public void DeleteUserMovie(string userId, string movieId) {
            var filter = Builders<Movie>.Filter.Eq(x => x.UserId, userId) &
                Builders<Movie>.Filter.Eq(x => x.Id, movieId);

            db.Delete<Movie>(COLLECTION_NAME, filter);
        }

        // PUT/PATCH
        public Movie UpdateUserMovie(string userId, Movie movie) {

            return null; // TODO: the new movie
        }
    }
}
