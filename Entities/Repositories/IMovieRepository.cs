﻿using MovieFavorites.Data.Entities;
using MovieFavorites.Data.Models;
using System.Collections.Generic;

namespace MovieFavorites.Data.Repositories {
    public interface IMovieRepository {
        IEnumerable<Movie> GetUserMovies(string userId);

        Movie GetUserMovie(string userId, string movieId);

        void AddUserMovie(Movie movie);

        void DeleteUserMovie(string userId, string movieId);

        Movie UpdateUserMovie(string userId, Movie movie);

        //void SearchUserMovies(string userId, MovieSearchModel model);
    }
}
