﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MovieFavorites.Data.Entities {
    public class Movie {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("user_id")]
        public string UserId { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("imdb_id")]
        public string ImdbId { get; set; }

        [BsonElement("rating")]
        public int Rating { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }
    }
}
