﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace MovieFavorites.Data {
    public class MongoCRUD : IMongoCRUD {

        private IMongoDatabase db;

        public MongoCRUD(string database) {
            var client = new MongoClient();
            db = client.GetDatabase(database);
        }

        public void Insert<T>(string collectionName, T record) {
            var collection = db.GetCollection<T>(collectionName);
            collection.InsertOne(record);
        }

        public List<T> ListAll<T>(string collectionName) {
            var collection = db.GetCollection<T>(collectionName);
            return collection.Find(new BsonDocument()).ToList();
        }

        public List<T> List<T>(string collectionName, FilterDefinition<T> filter) {
            var collection = db.GetCollection<T>(collectionName);
            var results = collection.Find(filter).ToList();
            return results;
        }

        public List<T> ListByField<T>(string collectionName, string fieldName, string value) {
            var filter = Builders<T>.Filter.Eq(fieldName, value);
            var results = List(collectionName, filter);
            return results;
        }

        public T FindByField<T>(string collection, string fieldName, string value) {
            var filter = Builders<T>.Filter.Eq(fieldName, value);            
            var result = Find(collection, filter);
            return result;
        }

        public T Find<T>(string collectionName, FilterDefinition<T> filter) {
            var collection = db.GetCollection<T>(collectionName);
            var result = collection.Find(filter).Limit(1).FirstOrDefault();
            return result;
        }

        public void Delete<T>(string collectionName, FilterDefinition<T> filter) {
            var collection = db.GetCollection<T>(collectionName);
            collection.DeleteOne(filter);            
        }
    }
}
