﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFavorites.Data {
    public interface IMongoCRUD {
        void Insert<T>(string table, T record);
        List<T> ListAll<T>(string table);
        List<T> List<T>(string table, FilterDefinition<T> filter);
        List<T> ListByField<T>(string table, string fieldName, string value);
        T Find<T>(string table, FilterDefinition<T> filter);
        T FindByField<T>(string collection, string fieldName, string value);
        void Delete<T>(string collectionName, FilterDefinition<T> filter);
    }
}
